import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HomeRoutingModule } from './home-routing.module'
import { HomeComponent } from './home.component'
import { LoginComponent } from './login/login.component'
import { CallbackComponent } from './callback/callback.component'
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { TabViewModule } from 'primeng/tabview';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ListboxModule } from 'primeng/listbox';
@NgModule({
  declarations: [HomeComponent, LoginComponent, CallbackComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, HomeRoutingModule, ButtonModule, InputTextModule, PasswordModule, TabViewModule, DropdownModule, CheckboxModule,RadioButtonModule, ListboxModule],
})
export class HomeModule { }
