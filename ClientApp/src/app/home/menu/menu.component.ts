import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  checkpage: number = 0
  constructor() { }

  ngOnInit(): void {
  }

  findpage(page:number){
    this.checkpage = 0
    if(page == 1){
      return this.checkpage = 1
    }if(page == 6){
      return this.checkpage = 6
    }
  }

}
