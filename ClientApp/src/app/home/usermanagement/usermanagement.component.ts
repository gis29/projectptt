import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usermanagement',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['./usermanagement.component.scss']
})
export class UsermanagementComponent implements OnInit {
  prefixs: Prefix[];
  selectedPrefix: Prefix;
  
  users: Data[];
  groups: DataGroup[];
  


  ///

  
  constructor() { 
    this.prefixs = [
      {name: 'นาง', code: ''},
      {name: 'นาย', code: ''},
      {name: 'นางสาว', code: ''}
    ]



   
   }
 


 
  ngOnInit(): void {
    this.users = [
      { username: 'xxxx123', code: '001001', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Assistant Manager', date: 'วว/ดด/ปป' },
      { username: 'xxxx124', code: '001002', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Senior', date: 'วว/ดด/ปป' },
      { username: 'xxxx125', code: '001003', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Supervisor', date: 'วว/ดด/ปป' },
      { username: 'xxxx126', code: '001004', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Assistant Manager', date: 'วว/ดด/ปป' },
      { username: 'xxxx127', code: '001005', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Senior', date: 'วว/ดด/ปป' },
      { username: 'xxxx128', code: '001006', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Senior', date: 'วว/ดด/ปป' },
      { username: 'xxxx129', code: '001007', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Officer', date: 'วว/ดด/ปป' },
      { username: 'xxxx130', code: '001008', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Supervisor', date: 'วว/ดด/ปป' },
      { username: 'xxxx131', code: '001009', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Assistant Manager', date: 'วว/ดด/ปป' },
      { username: 'xxxx132', code: '001010', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Assistant Manager', date: 'วว/ดด/ปป' }
    ],

      this.groups = [
        { groupName: 'ผู้บริหารระดับสูง', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
        { groupName: 'เจ้าหน้าที่จัดการเคส', detail: 'xxxxxxxxxxxxxxx', membersNumber: 8, date: 'วว/ดด/ปป' },
        { groupName: 'เจ้าหน้าที่มอนิเตอร์', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
        { groupName: 'ผู้บริหาร', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
        { groupName: 'เจ้าหน้าที่หน่วยงานภายนอก', detail: 'xxxxxxxxxxxxxxx', membersNumber: 5, date: 'วว/ดด/ปป' },
        { groupName: 'ผู้บริหารหน่วยงานภายนอก', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
        { groupName: 'เจ้าหน้าที่ติดตาม', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
        { groupName: 'เจ้าหน้าที่ตรวจสอบ', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
        { groupName: 'ผู้บริหาร PTT Digital', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
      ]


  }


  

  div1: boolean = true;
  div2: boolean = true;
  check:number = 0
  div1Function() {
    this.check = 1
    this.div1 = true;
    this.div2 = false;
  }

  div2Function() {
    this.check = 2
    this.div2 = true;
    this.div1 = false;
  }



}
export interface Data {
  username?: string,
  code?: string,
  name?: string,
  company?: string,
  position?: string,
  email?: string,
  phone?: string
  group?: string
  date?: string

}
export interface DataGroup {
  groupName?: string,
  detail?: string,
  membersNumber?: number
  date?: string

}



interface Prefix {
  name: string,
  code: string
}


