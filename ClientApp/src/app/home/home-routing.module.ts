import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { AxLoginGuard } from '@atlasx/core/authentication'

import { HomeComponent } from './home.component'
import { LoginComponent } from './login/login.component'
import { CallbackComponent } from './callback/callback.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { UsermanagementComponent } from './usermanagement/usermanagement.component'

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    // If you would like to required authenticate this route, uncomment the `canActivate`
    // to enable authentication before navigates to route.
    // , canActivate: [AxAuthenticationGuard] // <--- Enable HERE
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AxLoginGuard],
  },
  {
    path: 'callback',
    component: CallbackComponent,
  },
  {path: 'dashboard', component: DashboardComponent},
  {path: 'usermanagement', component: UsermanagementComponent}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
