import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'

import { AxAuthenticationModule } from '@atlasx/core/authentication'
import { AxConfigurationModule } from '@atlasx/core/configuration'
import { AxWebServiceUrl } from '@atlasx/core/http-service'

import { AppComponent } from './app.component'

import { environment } from '../environments/environment'
import { ArcgisjsapiProvider } from './gis/argisjsapi-provider';
import { LoginPageComponent } from './login-page/login-page.component';
import { UsermanagementComponent } from './usermanagement/usermanagement.component';
import {SplitterModule} from 'primeng/splitter';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ButtonModule} from 'primeng/button';
import {TabViewModule} from 'primeng/tabview';
import {InputTextModule} from 'primeng/inputtext';
import { DashboardComponent } from './dashboard/dashboard.component';
import {CardModule} from 'primeng/card';
import {MenubarModule} from 'primeng/menubar';
import { MenuComponent } from './menu/menu.component';
import {TableModule} from 'primeng/table';
import {ImageModule} from 'primeng/image';
import {CheckboxModule} from 'primeng/checkbox';
@NgModule({
  declarations: [AppComponent, LoginPageComponent, UsermanagementComponent, DashboardComponent, MenuComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ButtonModule,
    TabViewModule,
    InputTextModule,
    SplitterModule,
    CardModule,
    MenubarModule,
    TableModule,
    ImageModule,
    // Required register, if application use AtlasX configuration pattern.
    // It will load configuration before application initial startup.
    AxConfigurationModule,

    // Required register, if application use authentication.
    AxAuthenticationModule.forRoot(environment),
    // TabViewModule,
  ],
  providers: [
    // Required register, if application use AxAuthenticationModule or AxConfigurationModule.
    { provide: AxWebServiceUrl, useValue: environment.webServiceUrl },

    // Requried register, if application use ArcGIS API for JavaScript.
    ArcgisjsapiProvider,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
