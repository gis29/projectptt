import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usermanagement',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['./usermanagement.component.scss']
})
export class UsermanagementComponent implements OnInit {
  users: Data[];
  groups: Groupuser[];
  constructor() { }

  ngOnInit(): void {
    this.users = [
      { username: "xxxx1234 ", code: "0010001", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Assistant Manager, Manager", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "0010002", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Senior", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "0010003", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Supervisor", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "0010004", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Assistant Manager", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "0010005", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Senior", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "0010006", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Senior", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "0010007", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Officer", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "0010008", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Supervisor", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "0010009", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Supervisor", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "00100010", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Assistant Manager,Manager", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "00100011", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Senior", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "00100012", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Supervisor", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "00100013", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Senior", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "00100014", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Assistant Manager", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "00100015", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Senior", date: "วว/ดด/ปป" },
      { username: "xxxx1234 ", code: "00100016", name: "xxxxx xxxxxxxxxxx", company: "company", position: "xxxxxxxxxx", email: "work.work@email.com", phone: "088-123-4567", group: "Officer", date: "วว/ดด/ปป" },
    ]
    this.groups = [{ namegroup: 'ผู้บริหารระดับสูง', detail: 'xxxxxxxxxxxxxxx', count: 2, date: 'วว/ดด/ปป' }, { namegroup: 'เจ้าหน้าที่จัดการเคส', detail: 'xxxxxxxxxxxxxxx', count: 8, date: 'วว/ดด/ปป' }, { namegroup: 'เจ้าหน้าที่มอนิเตอร์', detail: 'xxxxxxxxxxxxxxx', count: 2, date: 'วว/ดด/ปป' }, { namegroup: 'ผู้บริหาร', detail: 'xxxxxxxxxxxxxxx', count: 2, date: 'วว/ดด/ปป' }, { namegroup: 'เจ้าหน้าที่หน่วยงานภายนอก', detail: 'xxxxxxxxxxxxxxx', count: 5, date: 'วว/ดด/ปป' }, { namegroup: 'ผู้บริหารหน่วยงานภายนอก', detail: 'xxxxxxxxxxxxxxx', count: 2, date: 'วว/ดด/ปป' }, { namegroup: 'เจ้าหน้าที่ติดตาม', detail: 'xxxxxxxxxxxxxxx', count: 2, date: 'วว/ดด/ปป' }, { namegroup: 'เจ้าหน้าที่ตรวจสอบ', detail: 'xxxxxxxxxxxxxxx', count: 2, date: 'วว/ดด/ปป' }, { namegroup: 'ผู้บริหาร PTT Digital', detail: 'xxxxxxxxxxxxxxx', count: 2, date: 'วว/ดด/ปป' },]
  }


}
export interface Data {
  username?: string,
  code?: string,
  name?: string,
  company?: string,
  position?: string,
  email?: string,
  phone?: string
  group?: string
  date?: string

}

export interface Groupuser {
  namegroup?: string,
  detail?: string,
  count?: number,
  date?: string
}
