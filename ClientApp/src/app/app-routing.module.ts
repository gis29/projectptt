import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { DashboardComponent } from './dashboard/dashboard.component'
import { LoginPageComponent } from './login-page/login-page.component'
import { MenuComponent } from './menu/menu.component'
import { UsermanagementComponent } from './usermanagement/usermanagement.component'

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'gis',
    loadChildren: () => import('./gis/gis.module').then((m) => m.GisModule),
    data: {
      systemId: 'GIS',
    },
  },
  {
    path: 'loginpage', component: LoginPageComponent
  },
  {path: 'usermanagement', component: UsermanagementComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'menu', component: MenuComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
